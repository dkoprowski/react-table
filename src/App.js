import React, { Component } from 'react';
import styles from './App.module.css';
import Table from './components/table/Table';

class App extends Component {
  render() {
    return (
      <div className={styles.app}>
        <Table
          initialRowsNumber={4}
          initialColumnsNumber={5}
        />
      </div>
    );
  }
}

export default App;
