import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ActionButton.module.css';

const propTypes = {
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};


class ActionButton extends Component {
    render() {
        return (
            <div className={styles.actionButton} onClick={this.props.onClick}>
                {this.props.text}
            </div>
        );
    }
}


ActionButton.propTypes = propTypes;


export default ActionButton;
