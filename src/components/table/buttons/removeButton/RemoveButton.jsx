import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RemoveButton.module.css';
import DeleteIcon from './delete.svg';
const propTypes = {
    id: PropTypes.string.isRequired,
    onRemove: PropTypes.func.isRequired
};


class RemoveButton extends Component {

    removeHandler = () => {
        this.props.onRemove(this.props.id)
    }

    render() {
        return (
            <div className={styles.button} onClick={this.removeHandler} title={'Delete'}>
                <img src={DeleteIcon} className={styles.buttonIcon} alt={'Delete icon'}/>
            </div>
        );
    }
}


RemoveButton.propTypes = propTypes;


export default RemoveButton;
