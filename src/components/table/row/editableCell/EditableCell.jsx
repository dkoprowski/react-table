import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './EditableCell.module.css';
import Cell from '../../cell/Cell'
const propTypes = {
    cellId: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onUpdateCell: PropTypes.func
};


class EditableCell extends Component {

    textChangeHandler = (event) => {
        this.props.onUpdateCell(this.props.cellId, event.target.value);
    }

    render() {
        return (
            <Cell>
                <input
                type="text"
                onChange={this.textChangeHandler}
                className={styles.input}
                value={this.props.value} />
            </Cell>
        );
    }
}


EditableCell.propTypes = propTypes;


export default EditableCell;
