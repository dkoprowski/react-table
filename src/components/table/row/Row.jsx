import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EditableCell from './editableCell/EditableCell';
import styles from './Row.module.css';
import Cell from '../cell/Cell';
import RemoveButton from '../buttons/removeButton/RemoveButton';

const propTypes = {
    rowId: PropTypes.string.isRequired,
    cells: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            rowId: PropTypes.string.isRequired,
            columnId: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        })).isRequired,
    onUpdateCell: PropTypes.func,
    onRowRemove: PropTypes.func,
    rowNumber: PropTypes.string
};


class Row extends Component {

    render() {
        const { cells, onUpdateCell, rowNumber } = this.props;
        return (
            <div className={styles.cellsContainer}>
                <Cell>
                    <span className={styles.rowNumber}>
                        {rowNumber}
                    </span>
                    <RemoveButton
                        id={this.props.rowId}
                        onRemove={this.props.onRowRemove}
                    />
                </Cell>
                {cells.map(cell => {
                    return (
                        <EditableCell
                            key={cell.id}
                            cellId={cell.id}
                            value={cell.value}
                            onUpdateCell={onUpdateCell}
                        />
                    )
                })}
            </div>
        );
    }
}


Row.propTypes = propTypes;


export default Row;
