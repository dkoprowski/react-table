import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RemoveButton from '../buttons/removeButton/RemoveButton';
import styles from './ColumnsHeader.module.css';
import Cell from '../cell/Cell';
import ActionButton from '../buttons/actionButton/ActionButton';


const propTypes = {
    columnIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    onColumnRemove: PropTypes.func.isRequired,
    onColumnAdd: PropTypes.func.isRequired,
    onExportCsvClick: PropTypes.func.isRequired
};


class ColumnsHeader extends Component {
    render() {
        return (
            <div className={styles.cellsContainer}>
                <Cell>
                <ActionButton
                    onClick={this.props.onExportCsvClick}
                    text={'Export CSV'}
                />
                </Cell>
                {this.props.columnIds.map((id, index) => (
                    <Cell key={id}>
                        <span className={styles.columnNumber}>
                            {index + 1}
                        </span>
                        <RemoveButton
                            id={id}
                            onRemove={this.props.onColumnRemove}
                        />
                    </Cell>
                ))}
                <Cell>
                    <ActionButton
                        onClick={this.props.onColumnAdd}
                        text={'Add column'}
                    />
                </Cell>
            </div>
        );
    }
}


ColumnsHeader.propTypes = propTypes;


export default ColumnsHeader;
