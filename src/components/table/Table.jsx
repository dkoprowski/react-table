import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Row from './row/Row';
import _ from 'lodash';
import ColumnsHeader from './columnsHeader/ColumnsHeader';
import Cell from './cell/Cell';
import ActionButton from './buttons/actionButton/ActionButton';
import { startCsvDownload } from '../../utils/csvGenerator';

const propTypes = {
    initialRowsNumber: PropTypes.number,
    initialColumnsNumber: PropTypes.number,
};


class Table extends Component {

    constructor(props) {
        super(props)

        const rowIds = [];
        const columnIds = [];
        const cells = [];
        for (let columnIndex = 0; columnIndex < props.initialColumnsNumber; columnIndex++) {
            const columnId = `c${columnIndex}`;
            columnIds.push(columnId);
        }
        for (let rowIndex = 0; rowIndex < props.initialRowsNumber; rowIndex++) {
            const rowId = `r${rowIndex}`;
            rowIds.push(rowId);
        }

        rowIds.forEach(rowId => {
            columnIds.forEach(columnId => {
                cells.push({ id: `${rowId}_${columnId}`, rowId, columnId, value: '' });
            })
        })

        this.state = {
            createdRowsCount: rowIds.length,
            createdColumnsCount: columnIds.length,
            rowIds,
            columnIds,
            cells
        }
    }

    addRowHandler = () => {
        this.setState(prevState => {
            const newRowsCount = prevState.createdRowsCount + 1;
            const newRowId = newRowsCount.toString();
            const newCells = prevState.columnIds.map((columnId) => {
                return {
                    id: `${newRowsCount}_${columnId}`,
                    rowId: newRowId,
                    columnId,
                    value: ''
                }
            });

            return {
                createdRowsCount: newRowsCount,
                rowIds: [...prevState.rowIds, newRowId],
                cells: [..._.cloneDeep(prevState.cells), ...newCells]
            };
        })
    }

    addColumnHandler = () => {
        this.setState(prevState => {
            const newColumnsCount = prevState.createdColumnsCount + 1;
            const newColumnId = newColumnsCount.toString();
            const newCells = prevState.rowIds.map((rowId) => {
                return {
                    id: `${rowId}_${newColumnId}`,
                    rowId,
                    columnId: newColumnId,
                    value: ''
                }
            });

            return {
                createdColumnsCount: newColumnsCount,
                columnIds: [...prevState.columnIds, newColumnId],
                cells: [..._.cloneDeep(prevState.cells), ...newCells]
            };
        })
    }

    updateCellHandler = (cellId, value) => {
        this.setState(prevState => {
            const newCells = prevState.cells.map(cell => {
                if (cellId === cell.id) {
                    return { ...cell, value }
                }
                else {
                    return cell;
                }
            });

            return {
                cells: [...newCells]
            }
        });
    }

    rowRemoveHandler = (rowId) => {
        this.setState(prevState => {
            const newCells = prevState.cells.filter(cell => cell.rowId !== rowId);
            const newRowIds = prevState.rowIds.filter(id => id !== rowId);
            return {
                rowIds: [...newRowIds],
                cells: [...newCells]
            }
        });
    }

    columnRemoveHandler = (columnId) => {
        this.setState(prevState => {
            const newCells = prevState.cells.filter(cell => cell.columnId !== columnId);
            const newColumnIds = prevState.columnIds.filter(id => id !== columnId);
            return {
                columnIds: [...newColumnIds],
                cells: [...newCells]
            }
        });
    }

    exportCsvHandler = () => {
        const { rowIds, cells } = this.state;
        const csvRows = [];

        rowIds.forEach(rowId => {
            csvRows.push(cells
                .filter(cell => cell.rowId === rowId)
                .map(cell => cell.value)
            );
        })

        let csv = '';
        csvRows.forEach(values => {
            csv += `${values.join(',')}${'\r\n'}`;
        });
        startCsvDownload(csv);
    }

    render() {
        const { rowIds, columnIds, cells } = this.state;
        return (
            <>
                <ColumnsHeader
                    columnIds={columnIds}
                    onColumnRemove={this.columnRemoveHandler}
                    onColumnAdd={this.addColumnHandler}
                    onExportCsvClick={this.exportCsvHandler}
                />
                {rowIds.map((rowId, index) => {
                    return (
                        <Row
                            key={rowId}
                            rowId={rowId}
                            cells={cells.filter(cell => cell.rowId === rowId)}
                            onUpdateCell={this.updateCellHandler}
                            onRowRemove={this.rowRemoveHandler}
                            rowNumber={(index + 1).toString()}
                        />
                    )
                })}
                <Cell>
                    <ActionButton
                        onClick={this.addRowHandler}
                        text={'Add row'}
                    />
                </Cell>
            </>
        );
    }
}


Table.propTypes = propTypes;


export default Table;
