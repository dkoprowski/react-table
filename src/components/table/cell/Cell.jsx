import React, { Component } from 'react';
import styles from './Cell.module.css';

class Cell extends Component {
    render() {
        return (
            <div className={styles.cell}>
                {this.props.children}
            </div>
        );
    }
}

export default Cell;
