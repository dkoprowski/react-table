export const startCsvDownload = (csvData) => {
    let csv = `data:text/csv;charset=utf-8,${csvData}`;
    window.open(encodeURI(csv));
}