## Table Component
Daniel Koprowski - 29.01.2019

I could move data logic to Redux or Mobx but I decided that leaving it on top-level component is good enough. State is normalized at some level. I was also wondering about usage of object/map to store data to simplify input/replace operations and prevent key duplication. Eventually I've done some non-nested data model.

Deploy: [dkoprowski.gitlab.io/react-table](https://dkoprowski.gitlab.io/react-table/)